using FastLapackInterface
using FastLapackInterface.LinSolveAlgo
using FastLapackInterface.SchurAlgo
using FastLapackInterface.QrAlgo
using LinearAlgebra
using Test

include("LinSolveAlgo_test.jl")
include("SchurAlgo_test.jl")
include("QrAlgo_test.jl")
