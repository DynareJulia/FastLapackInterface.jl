module FastLapackInterface

include("LinSolveAlgo.jl")
include("QrAlgo.jl")
include("SchurAlgo.jl")

end # module
